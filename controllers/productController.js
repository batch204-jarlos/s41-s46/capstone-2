const Product = require("../models/Product");
const User = require("../models/User");

// Controller for Admin Product Creation
// module.exports.productCreation = (reqBody) => {

// 		let newProduct = new Product ({
// 			name : reqBody.name,
// 			description : reqBody.description,
// 			price : reqBody.price
// 		});

// 		return newProduct.save().then ((product, error) => {

// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// }

 module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newProduct.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return true
                }
            })
        }
        
    });    
}


// Retrieve all active Products
module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// Retrieve all Products
module.exports.getAllActiveProducts = () => {
	return Product.find().then(result => {
		return result
	})
}



// Retrieve a specific product
module.exports.getProduct = (reqParams) => {



	return Product.findById(reqParams.productId).then(result => {
		return result
	// 	// console.log(result.id)
	// 	if (result.id === reqParams.productId) {
	// 		return result
	// 	} else {
	// 		return (`Product does not exist`)
	// 	}
	});

}

// Update product information (admin only)
module.exports.updateProduct = (reqParams, reqBody, data) => {

		
		return User.findById(data.id).then(result => {
			// console.log(result)


			if (result.isAdmin === true) {

				let updatedProduct = {
					name : reqBody.name,
					description : reqBody.description,
					price : reqBody.price
				};

			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

				if (error) {
					return false
				} else {
					return true
				}
			})

			} else {
				return false
			}
		})
}

module.exports.archiveProduct = (data, reqBody) => {

	return Product.findById(data.productId).then(result => {

		if (data.payload === true) {

			let updateActiveField = {
				isActive: reqBody.isActive
			}

			return  Product.findByIdAndUpdate(result._id, updateActiveField).then((product, err) => {


				if(err) {
					return false;

				} else {

					return true;
				}
			})

		} else {

			return false
		}

	})
}