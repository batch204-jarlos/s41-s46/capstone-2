const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");


// Creating an Order (non-admin)
module.exports.createOrder = (reqBody) => {

			// console.log(result)
		const newOrder = new Order ({
	
		userId: reqBody.userId,
		products: reqBody.products,
		totalAmount: reqBody.totalAmount

		});

		return newOrder.save().then((order, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})

	
}


// Retrieve User Details
module.exports.getUserOrders = (reqParams, data) => {

	// return Order.find({userId: data.userId}).then(result => {
	// 	console.log(result)
	// 	return result

		// console.log (result)

		return Order.findById(data.userId).then(result => {
			return result
			console.log(result)

	})
	// });
}


// Retrieving user details (with orders)

module.exports.getAllDetails = (reqParams, data) => {

	return User.findById(data.userId).then(result => {
		
		// console.log (result)

	if (result.id === data.userId) {

		return Order.find({userId: data.userId}).then(result => {
			return (result)
			// return ([{data}, {result}])
			console.log(result)
		})
	} else {
		return false
	}

	})

}


// Retrieving all orders (admin only)
module.exports.getAllOrders = () => {

	return Order.find({}).then(result => {

		return result
		console.log(result)
	})
}
