const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// Create Product Functionality (Admin only)
// router.post("/add", auth.verify, (req, res) => {

// 	const isAdmin = auth.decode(req.headers.authorization).isAdmin

// 	console.log(isAdmin);

// 	if (isAdmin) {
	
// 		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
// 	} else {
// 		return false
// 	}
// });

// Route for retrieving all active the product
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all the product
router.get("/admin/all", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
});

router.post("/add", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})


// Route for retrieving a specific product
router.get("/:productId", (req, res) => {
		console.log("2")
		console.log(req.params.productId)
		console.log(req.params)
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});




// Route for updating a product (admin only)
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log("1")
	productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
})


// Route for archiving a product (admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {

	console.log(req.params)

	const data = {
		productId : req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data, req.body).then(resultFromController => res.send(resultFromController))
});


module.exports = router;