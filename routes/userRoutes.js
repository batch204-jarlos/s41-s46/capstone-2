const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


//Route for checking if the user's email already exist in the database
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// User Registration route
router.post("/register", (req,res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))

});


// User Login route
router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// User details
router.get("/details", auth.verify, (req,res) => {
	// console.log(req.headers)

	const userData = auth.decode(req.headers.authorization);
	// console.log(userData.id)

	// if (userData.id == req.body.userId) {
	// 	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
	// } else {
	// 	res.send (`User not found`)
	// }
	console.log(userData);
	console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))

});






// Retrieve user details
module.exports = router;