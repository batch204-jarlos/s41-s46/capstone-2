const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require('../auth.js');

// Route for creating an order
router.post("/add", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData.id)
	// console.log(req.params.userId)

	if(userData.isAdmin == true) {
		return false
	} else if (userData.id) {
		orderController.createOrder(req.body).then(resultFromController => res.send(resultFromController))
	}else {
		return false
	}
	
});

// User Details Route
router.get("/:userId/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData.id)
	// console.log(req.params)
	// console.log(req.params.userId)

	if (userData.id === req.params.userId) {
		orderController.getUserOrders({id: userData.id}, req.params).then(resultFromController => res.send(resultFromController))
	} else {
		return false
	}
});


// Get User All Info (details and orders)
router.get("/:userId/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData.id)

	if (userData.id === req.params.userId) {
		orderController.getAllDetails({id: userData.id}, req.params).then(resultFromController => res.send(resultFromController))
	} else {
		return false
	}
});


// Get all Orders (admin only)
router.get("/all", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if (isAdmin) {
		orderController.getAllOrders().then(resultFromController => {
			res.send(resultFromController)
		})
	} else {
		return false
	}
});


module.exports = router;